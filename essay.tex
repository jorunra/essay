\documentclass[a4paper,11pt]{article}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{epsfig, times}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{ifikompendiumforside}
\usepackage{textcomp}
\usepackage{graphicx, chngpage}
\usepackage{enumitem}
\usepackage{algpseudocode,algorithmicx,algorithm}
\usepackage[margin=3.5cm]{geometry}
\usepackage[backend=biber,style=numeric-comp]{biblatex}
\setlength{\parindent}{0pt}

\title{Similarity search in protein databases with the Intel Xeon Phi coprocessor}
\author{Jorun Ramstad}

\bibliography{references}

\begin{document}
\ififorside{}

\maketitle
\tableofcontents
\newpage

\pagenumbering{arabic}

\section{Introduction}
In bioinformatics an important and time consuming task is to compare a new
protein sequence with unknown properties to a set of sequences in a large
database containing millions of sequences. This to find sequences in the
database that matches the new sequences optimal, thus most likely indicating
that they are related through a common ancestor known as homologs. Usually these
sequences share many biological properties, and the information known about one,
can therefore often be transferred to the other related sequences too. \\

The two most common algorithms used to solve this task are Basic Local Alignment
Search Tool (BLAST) by Altschul et al. \cite{altschul1990basic} and Smith and
Waterman (SW) \cite{smith1981identification}. The first one uses a heuristic
approach, while the second uses the principles of dynamic programming. A number
of different tools exists that utilizes either of the algorithms. Most
of them are made to run on CPU's and can be run on a variety of standard
computers.  Because of time consumption, parallelization and optimal use of
resources are now high priority and several new tools have been published in
the last decade. Some implementations are still working well on CPU's, while
others are made for specialized hardware, to achieve an even higher
performance. Some examples are graphics processing units (GPU) and the Cell
Broadband Engine Architecture \cite{maeurer2005introduction} (Cell) on
PlayStation 3 that offers a unique assembly of thread-level and data-level
parallelization option.\\

In January 2013, Intel released the Xeon Phi coprocessor as a serious contender
in the high performance programming business. It is designed to tackle highly
parallel problems, and is a promising hardware to run and optimize
sequence alignment implementations on. The question is, will previous parallel
optimizations for regular CPUs, GPUs or PlayStation work well on the Xeon
Phi, and if any of them  are the optimal choice? \\

To answer these questions a look on how to fully utilize the Xeon Phi
coprocessor is required, together with research on how previous implementations
of both BLAST and Smith-Waterman utilize parallelization. \\

Recently Liu and Smith \cite{swaphi} released a new tool, SWAPHI, that utilize
a Smith-Waterman approach implemented on an Intel Xeon Phi. This tool is
interesting to look into to see how they utilize the Xeon Phis unique design,
and is a good implementation to compare my future alignment tool against,
together with BLAST\cite{altschul1990basic}, CUDASW++ latest version by Liu et
al. \cite{liu2013cudasw++} and Rognes'\cite{rognes2011faster} tool SWIPE. \\

But first a look into the principles used when comparing protein sequences, the
format, both for input and output, and how to determining the similarity
between sequences. \\

\newpage
\section{Background}
\subsection{Global and local alignment}
To find related sequences, local alignment is used to identify regions of
similarity within a long sequence. This to find sub-sequences in the database
which are similar to subsequences in the query.
Given the two sequences:
\begin{center}
\begin{tabular}{ccccccccccccc}
    F&T&F&T&A&L&I&L&L&A&V&A&V \\
    F&T&A&L&L&L&A&A&V \\
\end{tabular}
\end{center}

The global alignment contains both sequences in whole with the first and last
letter mapped together, and the rest is aligned to the optimal match.

\begin{center}
\begin{tabular}{ccccccccccccc}
    F&T&F&T&A&L&I&L&L&A&V&A&V \\
    F&-&-&T&A&L&-&L&L&A&-&A&V \\
\end{tabular}
\end{center}


For local alignment the subsequences that matches the best is returned. In this
case:

\begin{center}
\begin{tabular}{cccccccccc}
    F&T&A&L&I&L&L&-&A&V \\
    F&T&A&L&-&L&L&A&A&V \\
\end{tabular}
\end{center}

The reason local alignment is preferred above to global is that the query may
be significantly shorter than the majority of the database sequences or that it
is only the regions that are highly similar that are interesting, not the parts
that do not match. \\

\subsection{Substitution scoring matrix} Usually a substitution scoring matrix
is used to calculate the scores of the similarity between the query and each of
the database sequences. The scoring matrix describes the probability rate of
which an amino acid \textit{a} in a sequence is changed to another amino acid
\textit{b} in a certain (evolutionary) time.  \cite{eidhammer2004protein} When
comparing sequences, similar amino acid entries should get a positive score,
and negative when entries differs. The negative score for dissimilarity should
reflect the statistically probability rate of the change from one amino acid to
the other. This will result in a highest similarity score for the entry in the
database that is the best match.  \\

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.5]{BLOSUM62.png}}
\caption[caption]{The BLOSUM62 matrix \cite{blosum62}.\label{fig1}}
\end{figure}

The two most widely used series of scoring matrices are PAM and BLOSUM. E.g. the
BLOSUM62 matrix shown in figure \ref{fig1} on page \pageref{fig1} from
wikipedia\cite{blosum62}. They both contain the two aspects all scoring matrices
should reflect, the degree of "biological relationship" between the amino acids,
and the probability  that two amino acids occur in homologous positions in
sequences that have a common ancestor, or that one is the ancestor of the
other\cite{eidhammer2004protein}. \\

\subsection{Gap penalty}
To get the best match there are often a need to either insert or delete an amino
acid entry to align the query sequence with a database entry. This change also
needs to be considered when calculating the score of similarity, and is referred
to as gap penalty.  The gap penalty is usually assigned by an affine function
that give an initial penalty for a gap opening, and an additional penalty which
increases in correspondence with the length of the gap. \\

\subsection{Protein database}
A common database for proteins is \emph{GenBank} \cite{benson2008genbank} and
it contained over 150 billion nucleotide bases in more than 162 million
sequences in 2013. It is produced and maintained by the National Center for
Biotechnology Information (NCBI) as part of the International Nucleotide
Sequence Database Collaboration (INSDC). Also built by NCBI is The Reference
Sequence (\emph{RefSeq}) database \cite{pruitt2007ncbi}.  It differs from
\emph{GenBank} since it only provides a single record for each natural
biological molecule, i.e. DNA, RNA or protein, from organisms. \\

A second database is the UniProtKB/Swiss-Prot database
\cite{bairoch2005universal}. It is a manually annotated, non-redundant protein
sequence database. The aim of UniProtKB/Swiss-Prot is to provide all known
relevant information about a particular protein. Annotation is regularly
reviewed to keep up with current scientific findings. The manual annotation of
an entry involves detailed analysis of the protein sequence and the scientific
literature. From the release 2014\_05 of 14-May-14 \cite{uniprot} the
UniProtKB/Swiss-Prot database contains 545388 sequence entries, comprising
193948795 amino acids abstracted from 228536 references.

\subsection{Input format}
To be able to do an alignment search, the database and the query have to be
given to the alignment tool to be loaded into memory. The common format used for
searches in protein databases are the FASTA \cite{lipman1985rapid} format that
originating from the alignment tool FASTP.  It is a text based format in which
the amino acids are represented by single letter codes. Each sequence in FASTA
format begins with a single line description followed by the sequence data. The
description line is distinguished from the sequence data by an initial '>'
symbol. The identifier of the sequence is the word directly following the '>'
and the rest of the line is the description, both of these are optional
information. The end of the sequence is recognized by the start of the next
sequence beginning with '>'. \\

An example sequence from wikipedia \cite{fasta} in FASTA format:\\

\begin{adjustwidth}{1.5 cm}{0 cm}
\small{
>MCHU - Calmodulin - Human, rabbit, bovine, rat, and chicken\\
ADQLTEEQIAEFKEAFSLFDKDGDGTITTKELGTVMRSLGQN\\
PTEAELQDMINEVDADGNGTIDFPEFLTMMARKMKDTDSEE\\
EIREAFRVFDKDGNGYISAAELRHVMTNLGEKLTDEEVDEMI\\
READIDGDGQVNYEEFVQMMTAK\\
}
\end{adjustwidth}

\subsection{Methods} After the sequence database is read into memory a search
is performed, either with BLAST or Smith-Waterman, to find the optimal local
alignment.  BLAST is time efficient, but does not guarantee to find the optimal
match in all cases because of the reduced sensibility in the heuristic
algorithm.  Smith-Waterman on the other hand is a highly reliable algorithm,
and will always give the formally correct sequence having the highest optimal
alignment score, but it is more time-consuming compared to BLAST. \\

\subsection{Result format}
The result from an alignment search is often returned in a human readable
format, e.g. html, xml or plain text. There is no standard, but a possibility
is to use a standard format such as the Sequence Alignment/Map (SAM). SAM is a
text based and stores data in tab delimited ASCII columns with a fixed number
of mandatory fields that describes key information about the alignment. SAM
allows an optional header to describe the file and to add additional
information and columns, e.g.  information about the reference sequence or
aligner specific information. SAM is flexible enough to store alignment
information generated by various alignment tools, yet simple enough to easily
convert to existing alignment formats. The file size is compact, and allows
most of the operations on the alignment to work on a stream without loading the
whole alignment into memory.

Since SAM may be slow to parse and is made to be human readable, the binary
equivalent format BAM may also be used as output from an alignment search,
especially if the result is going to be post processed.


\newpage
\section{Intel Xeon Phi coprocessor}
\subsection{Technical specifications} A few key technical specifications about
the Xeon Phi from the Best Practice Guide Intel Xeon Phi v1.1 by Barth et al.
\cite{xeonguide2013}, supplemented by Jeffers and Reinders
\cite{jeffers2013intel} book, is described below. \\

The Xeon Phi is a coprocessor made to work together with a "host" processor,
mainly the Intel Xeon. They are connected via a high-speed serial computer
expansion bus, the PCI Express (PCIe).  The Xeon Phi consists of up to 61
cores, and they are connected by a high performance on-die bidirectional
interconnect. The processor runs a full service Linux operating system, and
therefore supports Intel development tools, e.g. C/C++ compilers, MPI and
OpenMP that may be highly useful when parallelizing the alignment search in
protein databases.  \\

Each core are based on the Intel Pentium processors family with a scalar
unit and a vector unit, with an in-order architecture. The only big difference
is the vector unit which will be described below. Each
core fetches and decodes instructions from four hardware threads and per cycle
each core can execute two instructions, one on the U-pipe and one on the
V-pipe. Not all instructions can be executed by the V-pipe, e.g. vector
instructions, and the V-pipe is idle for a cycle if the instruction in line is
a vector instruction. An overview of the Xeon Phi architecture is shown in
Figure \ref{fig3}.\\

\begin{figure}[!h]
\centerline{\includegraphics[scale = 0.4]{MIC.png}}
\caption[caption]{The Xeon Phi architecture overview. \label{fig3}}
\end{figure}

The new Vector processing Unit (VPU) has a new instruction set that utilize a
dedicated 512-Bit wide vector floating point unit (VPU) on each core. Previous
Intel SIMD extensions is not supported. New instructions, including
gather/scatter, fused multiply-add, masked vector instruction etc. which
allows more loops to be parallelized on the coprocessor than on an Intel Xeon
based host, is available. With the SIMD width of 64-Byte (512-Bit) all data
should be aligned to 64-Byte to achieve a good performance. Most vector
instructions on the Xeon Phi has a 4-clock latency with a 1 clock throughput.
To achieve autovectorization pragmas like \textit{\#pragma vector aligned} or
\textit{\#pragma simd} may be used.  Autovectorization is enabled at default
optimization level \textit{-O2}.  The VPU includes the Extended Math Unit (EMU)
that makes it possible to executes 16 32-Bit integer operations or 8
double-precision floating point operations per cycle. \\

The Xeon Phis cache hierarchy consist of the L1 cache that each core utilizes
solely, and a shared L2 cache and tag directory for all of the cores.  The L1
cache consist of a 32 KB L1 instruction cache and 32 KB L1 data cache. It has a
load-to-use latency of 1 cycle, which means that an integer value loaded from
the L1 cache can be used in the next clock cycle by an integer instruction
(vector instructions have different latencies than integer instructions). The
L2 cache contributes 512 KB to the global shared L2 cache storage, inclusive of
the L1 data and instruction cache. The effective total L2 size of the chip is
only 512 KB if every core shares exactly the same code and data in perfect
synchronization, if no cores share any data or code the effective total L2 size
of the chip is up to 31 MB. The actual size of the workload-perceived L2
storage is a function of the degree of code and data sharing among cores and
threads. The raw latency for the L2 cache is 11 clock cycles. \\

To connect all cores the Xeon Phi uses a ring interconnect via the Core Ring
Interface (CRI).  It hosts the global shared L2 cache storage and the tag
directory. Figure \ref{fig3} on page \pageref{fig3} illustrates the Xeon Phis
architecture with the Ring interface marked as a gray ring traveling in each
direction. The blue area is to illustrate the shared L2 cache and tag
directory.  \\

The instructions my future tool need to perform a sequence alignment search is
adding (add) and subtracting (sub), the option of returning the largest (max)
or smallest (min) value, and a way of comparing (compare) two values. \\

\subsection{Gaining optimal performance}
A single Xeon Phi core is slower than a Xeon core due to lower clock frequency,
smaller caches and lack of sophisticated features such as out-of-order execution
and branch prediction. To fully exploit the processing power of a Xeon Phi,
parallelism on both instruction level (SIMD) and thread level (OpenMP) is
needed. Xeon Phi can only perform memory reads/writes on 64-byte aligned data.
Any unaligned data will be fetched and stored by performing a masked unpack or
pack operation on the first and last unaligned bytes. This may cause performance
degradation, especially if the data to be operated on is small in size and
mostly unaligned. \\

Each Xeon Phi core has a 512-bit VPU unit which is capable of performing 16SP
flops or 8 DP flops per clock cycle. VPU units are also capable of Fused
Multiply-Add or Fused Multiply-Subtract operations which effectively double the
theoretical floating point performance. Vectorization is a form of
data-parallel programming which makes the processor perform the same operation
simultaneously on \textit{N} data elements of a vector. A vector may be looked
at as a one dimensional array of scalar objects. Vectorization of an application
can give as much as 8x (double precision) or 16x (single precision float)
speedup on the Intel Xeon Phi cprocessor in the perfect case. It is not
realistic to expect to fully reach the potential speedup, but without
vectorization the application would have a lot to gain performance wise. \\

When it comes to memory, the Xeon Phi has a 8 GB capacity and a memory channel
interface speed of 5.5 gigatransfers per second (GT/s) on a 60 cores
coprocessor. There are 8 memory controllers each accessing two memory channels
on the coprocessor. Each memory transaction is 4 byte of data, resulting in
$5.5$ GT/s times $4$ bytes or $22$ GB/s per each $16$ channels, giving a maximum
transfer rate of $352$ GB/s. An effective peak of 50 to 60 percent is what is
realistic to expect. \\

To gain optimal performance the following then needs to give a positive answer:

\begin{itemize}
    \item Scaling: How well does the application scale on multiple cores?
    \item Vectorization: Are the application making strong use of the
        vectorization?
    \item Memory usage: Is data aligned to 64 bit and cache assigned well?
\end{itemize}

\newpage
\section{Existing tools}
\subsection{BLAST}
BLAST uses a technique designed for solving a problem more quickly when classic
methods are too slow, or for finding an approximate solution when classic
methods fail to find any exact solution, called heuristic.  This is achieved by
trading optimality, completeness, accuracy, or precision for speed. In a way,
it can be considered a shortcut.  It does not guarantee the optimal alignment,
but with a heuristic that approximates the Smith-Waterman algorithm the result
is more that acceptable.  It does not compare either sequences in its entirety,
but rater locates short matches between the two. The main idea of
BLAST is that statistically significant alignments often contains segment pairs
that do not increase its score while either extending or shortening down its
length, also known as high-scoring segment pairs (HSP). \\

The first step of BLAST is to find matching segment pairs, word pairs, with
length \textit{w}, that score at least \textit{T}. The words to compare with
the database sequences are found by matching all possible \textit{w} letter
words out of the 20 amino acids to the query sequence, and save the words that
have a score higher than \textit{T} for at least one word in the query
sequence. For protein sequences the word length, \textit{w}, is usually 3. The
database is then searched for occurrences of the saved words from the query
sequence to find hits.  The hits is extended to high-scoring segment pairs to
check if they score higher than a threshold \textit{V}. This threshold is
determined such that there is reason to believe homology
\cite{eidhammer2004protein}. \\

Depending on \textit{T} and \textit{w}, the sensitivity is determined. While
increasing \textit{T} the runtime of the search decreases, since fewer
word pairs is found and extended. This will also decrease the sensitivity
as word pairs might be overlooked. The last step is using dynamic programming
to align the HSPs that score more than the given threshold to introduce gaps.
\\

The original BLAST only generates ungapped alignments individually, including
the initially found HSPs, even when more than one HSP is found in a
database sequence. Later versions of BLAST produces a single alignment with
gaps that can include all of the initially found HSP regions.\cite{Wiki-BLAST}

\subsection{Smith-Waterman} Dynamic programming can be used to find optimal
alignment, both global and optimal local, with a few changes to the algorithm.
Needleman and Wunsch \cite{needleman1970general} where the first to use dynamic
programming in bioinformatics to find optimal global alignment.  Their
algorithm provided the foundation upon the first approach for optimal local
alignment, done by Smith and Waterman in 1981, hence the later well known
Smith-Waterman algorithm\cite{smith1981identification}. The complexity of the
algorithm where $O(m^2n)$, which where later improved by Gotoh
\cite{gotoh1982improved} to run at $O(mn)$.\\

Dynamic programming is often used to solve optimization problems where the
problem can have a number of feasible solutions, and the desired solution is
the \textit{best} one of the them. The basic idea is that result found earlier
in the solution process are used in later calculations by storing it, e.g. in
a matrix. \\

The Smith-Waterman algorithm is divided into two parts. First identify the
highest possible score using dynamic programming, utilizing both a scoring
matrix \textit{s}, like BLOSUM62, and affine gap penalty \textit{W}. If the
score for a matrix cell is negative, the cell score is set to zero.

$$
H(i,0) = 0,\; 0\le i\le m.
$$
$$
H(0,j) = 0,\; 0\le j\le n.
$$
$$
H(i,j) = \max \begin{Bmatrix}
0  \\
H(i-1,j-1) + \ s(a_i,b_j) & \text{Match/Mismatch} \\
\max_{k \ge 1} \{ H(i-k,j) + \ W_k \} & \text{Deletion} \\
\max_{l \ge 1} \{ H(i,j-l) + \ W_l \} & \text{Insertion}
\end{Bmatrix},
$$\\

$\text{where }1\le i\le m \text{ and } 1\le j\le n.$\\

An example from wikipedia \cite{SW-Wiki} with the two sequences,

\begin{center}
\begin{tabular}{rcccccccc}
    Database sequence: &A&C&A&C&A&C&T&A \\
    Query sequence: &A&G&C&A&C&A&C&A
\end{tabular}
\end{center}

calculating with a simplified scoring matrix $s(a, b) = +2$ if $a = b$ (match),
$-1$ if $a \neq b$ (mismatch), gives the resulting matrix

$$
H =
\begin{pmatrix}
  & - & A & C & A & C & A & C & T & A \\
- & \color{blue}0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
A & 0 & \color{blue}2 & 1 & 2 & 1 & 2 & 1 & 0 & 2 \\
G & 0 & \color{blue}1 & 1 & 1 & 1 & 1 & 1 & 0 & 1 \\
C & 0 & 0 & \color{blue}3 & 2 & 3 & 2 & 3 & 2 & 1 \\
A & 0 & 2 & 2 & \color{blue}5 & 4 & 5 & 4 & 3 & 4 \\
C & 0 & 1 & 4 & 4 & \color{blue}7 & 6 & 7 & 6 & 5 \\
A & 0 & 2 & 3 & 6 & 6 & \color{blue}9 & 8 & 7 & 8 \\
C & 0 & 1 & 4 & 5 & 8 & 8 & \color{blue}11 & \color{blue}10 & 9 \\
A & 0 & 2 & 3 & 6 & 7 & 10 & 10 & 10& \color{blue}12
\end{pmatrix}.
$$\\

Then the alignment(s) is identified by going backward from the highest score
following the highest entry upward in the matrix until a 0 is the only next
entry. A matrix with arrows is shown below to better illustrate the alignment.

$$
T =
\begin{pmatrix}
  & - & A & C & A & C & A & C & T & A \\
- & \color{blue}0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
A & 0 & \color{blue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow & \nwarrow \\
G & 0 & \color{blue}\uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \nwarrow & \uparrow \\
C & 0 & \uparrow & \color{blue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \color{blue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow & \nwarrow \\
C & 0 & \uparrow & \nwarrow & \uparrow & \color{blue}\nwarrow & \leftarrow & \nwarrow & \leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \nwarrow & \uparrow & \color{blue}\nwarrow & \leftarrow & \leftarrow & \nwarrow \\
C & 0 & \uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \color{blue}\nwarrow & \color{blue}\leftarrow & \leftarrow \\
A & 0 & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \uparrow & \nwarrow & \color{blue}\nwarrow
\end{pmatrix}.
$$\\

This gives the optimal alignment following the arrows.

\begin{center}
\begin{tabular}{rccccccccc}
    Database sequence: &A&-&C&A&C&A&C&T&A \\
    Query sequence: &A&G&C&A&C&A&C&-&A
\end{tabular}
\end{center}

A diagonal arrow reflect a match between the query and the database sequence, a
upward arrow implies a deletion and a left arrow implies an insertion. \\

A Smith-Waterman tool that is one of the fastest on the marked for a CPU is
Rognes' \cite{rognes2011faster} tool SWIPE. The algorithm is implemented on Intel
processors with SSSE3 with parallelization over multiple database sequences.
Instead of aligning one database sequence against the query sequence at a time,
residues from multiple database sequences are retrieved and processed in
parallel.  Rapid extraction and organization of data from the database sequences
have made this approach feasible. The approach also involves computing four
consecutive cells along the database sequences before proceeding to the next
query residue in order to reduce the number of memory accesses needed. \\


A second tool that utilizes Smith-Waterman with good results is CUDASW++ 3.0
\cite{liu2013cudasw++}. It is coupling CPU and GPU SIMD instructions and
conducting concurrent CPU and GPU computations. To balance the runtime
differences of the CPU and the GPU the distribution of sequences is done
dynamically by their compute power. The optimizations done for the CPU is
employed by the steaming SIMD extension (SSE)-based vector execution units and
multithreading. For the GPU a SIMD parallelization approach using PTX SIMD
video instruction is used to obtain more data parallelization.

\newpage
\section{Conclusion}
Sequence similarity search utilizing the Smith-Waterman algorithm have been
optimized in several different implementations and have gained a significant
speed boost to the point that it is now on equal footing with BLAST when
comparing speed. Combined with the guarantee of the optimal sequence a
Smith-Waterman approach is the obvious choice to implement on the Xeon Phi.
\\

Because of the vectorization needed on the Xeon Phi, optimization techniques
that utilizes SIMD vectorization seems interesting to look further into and
test. Since Smith-Waterman approaches have gained performance on GPUs it
is likely that it will benefit from the Xeon Phi as well, since GPUs and the
Intel Xeon Phi coprocessor share a subset of what can be accelerated by scaling
combined with vectorization or bandwidth. Tuning especially done for GPUs may
not be optimal for the Xeon Phi, but with the right tuning, the same result or
better may be achieved. \\
\newpage
\printbibliography
\end{document}
